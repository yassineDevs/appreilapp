import { Router } from '@angular/router';
import { APPREILS } from './../mocks/appreils.mock';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-appareil',
  templateUrl: './view-appareil.component.html',
  styleUrls: ['./view-appareil.component.scss']
})
export class ViewAppareilComponent implements OnInit {

  appreils = APPREILS;

  constructor(private router:Router) {
    
  }

  ngOnInit() {

  }

  onLogout(){
    //clear localstorage varialbles
    localStorage.clear(); 
    //redirection vers le auth 
    this.router.navigate(['auth']);
  }

  OnSwitch(index){
    this.appreils[index].status = this.appreils[index].status == 1 ? 0 : 1;
  }
  
  onAllumer(){
    for (var i in this.appreils) {
      this.appreils[i].status = 1;
    }
  }

  onEteindre(){
    for (var i in this.appreils) {
      this.appreils[i].status = 0;
    }
  }

}
