
import { AppreilService } from './../services/appreil.service';
import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-appareil',
  templateUrl: './appareil.component.html',
  styleUrls: ['./appareil.component.scss']
})
export class AppareilComponent implements OnInit {

  @Input('name')  appareilName:string;
  @Input('status') appareilStatus:string;
  @Input('index') indexAppreil:number;
  action:string;
  @Output() onClickEvent = new EventEmitter(); 
  
  constructor(private _appreilService:AppreilService) { }

  onSwitch():void{
    if(this.appareilStatus==='eteint')
    {
      this.action='e';
    }
    else{
      this.action='a';
    }
    this.onClickEvent.emit;//bila chofe gole l parent rah taclickat btn 
  }

  ngOnInit(){ 
    this.action= this.appareilStatus =='allumer' ? 'e':'a';
  }

}
