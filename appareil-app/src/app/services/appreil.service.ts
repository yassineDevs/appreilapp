import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppreilService {

  appareils = [
    {
      'name':'telephone',
      'status':'allumer'
    }, 
    {
      'name':'ordinateur',
      'status':'etteint' 
    }
  ]
  
  getAll(){
    return this.appareils;
  }

  allumer(index:number):void{
      // console.log('allumer : ',index);
      this.appareils[index].status='allumer';
  }

  eteindre(index:number):void{
    // console.log('eteindre : ',index);
    this.appareils[index].status='etteint';

    
  }


  constructor() { }
}
