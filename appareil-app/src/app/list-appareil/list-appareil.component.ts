import { AppreilService } from './../services/appreil.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-appareil',
  templateUrl: './list-appareil.component.html',
  styleUrls: ['./list-appareil.component.scss']
})
export class ListAppareilComponent implements OnInit {
  
  appareils:any;

  constructor(private serviceAppreil:AppreilService) { 
  }
  
  ngOnInit() {
    this.appareils = this.serviceAppreil.getAll();
  }

  onSwitchAppreil(){
    console.log("onswitch clicked ");
  }
}
