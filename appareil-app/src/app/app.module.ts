import { AuthGuard } from './guards/auth.guard';
import { DashboardGuard } from './guards/dashboard.guard';
import { ViewAppareilComponent } from './view-appareil/view-appareil.component';
import { AuthComponent } from './auth/auth.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { AppComponent } from './app.component';

const routes: Routes = [
  {
    path:'',
    redirectTo:'auth',
    pathMatch:'prefix'
  },
  {
    canActivate:[AuthGuard],
    path:'auth',
    component:AuthComponent,
  },
  {
    canActivate:[DashboardGuard],
    path:'dashboard',
    component:ViewAppareilComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    ViewAppareilComponent 
  ],
  imports:[
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
