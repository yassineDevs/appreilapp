import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  public errMsg = '';
  public err=false;

  constructor(private router:Router) { }

  ngOnInit() {
  }

  onLogin(username,pass){
    //console.log(username.value,pass.value);
    if(username.value=='admin' && pass.value=='admin123'){
    
      //on va engister l'utilisateur sur le localStorage 
      localStorage.setItem('username',username.value);
      localStorage.setItem('password',window.btoa(pass.value));
      
      //redirection vers dashboard 
      this.router.navigate(['dashboard']);

    }else{
      this.errMsg = 'invalid credentials pls try again !! ';
      this.err=true;
    }
  }
}
